import java.util.Scanner;

public class Application{
	
	public static void main(String[] args){
		Student[] section3 = new Student[3];
		section3[0] = new Student();
		section3[1] = new Student();
		section3[2] = new Student();
		
		section3[0].name = "Bro";
		section3[0].grade = 45;
		section3[0].course = "English";
		
		section3[1].name = "Nerd";
		section3[1].grade = 92;
		section3[1].course = "Science";
		
		section3[2].name = "Joe";
		section3[2].grade = 60;
		section3[2].course = "Computer Science";
		
		System.out.println(section3[0].toString());
		System.out.println(section3[1]);
		System.out.println(String.valueOf(section3[2]));
		
		section3[0].dropOut();
		section3[1].dropOut();
		section3[2].dropOut();
		
		System.out.println();
		
		System.out.println(section3[0].toString());
		System.out.println(section3[1]);
		System.out.println(section3[2]);
		
		
	}
}